<?php

namespace ArgilData\ArgilBlade\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Blade;
use Illuminate\View\Compilers\BladeCompiler;
use Illuminate\View\ComponentAttributeBag;

use ArgilData\ArgilBlade;
use ArgilData\ArgilBlade\View\Components\{
    Button,
    //Input,
};

class ArgilBladeServiceProvider extends ServiceProvider 
{

    public function boot() {
        //dd('ArgilBlade Service Provider');


        //views
        $this->loadViewsFrom(__DIR__ . '/../views', 'adda');
        $this->loadViewsFrom(__DIR__.'/../views/components', 'adda');
        $this->loadViewsFrom(__DIR__.'/../../resources/views', 'adda');
        

        //publish
        $this->publishes([
            __DIR__.'/../config/argil-blade.php' => config_path('argil-blade.php'),
            __DIR__."/../../public" => public_path('adda'),
            __DIR__."/../../dist/argil-blade.css" => public_path('adda/argil-blade.css'),
            __DIR__."/../../dist/argil-blade.es.js" => public_path('adda/argil-blade.es.js'),
            __DIR__."/../../dist/argil-blade.umd.js" => public_path('adda/argil-blade.umd.js'),
            // if we want to publish the components views inside the project
            //__DIR__.'/../views' => resource_path('views/vendor/vui-kit')
        ], 'adda');

        
        // BLADE COMPONENTS
        Blade::component('btnsrctest', Button::class);
        Blade::component('adda-button', Button::class);

        // LIVEWIRE COMPONENTS
        // Livewire::component('some-component', SomeComponent::class);

    }

    public function register() {

        $this->app->singleton('argil-blade', ArgilBlade::class);

    }

}


