@props([
    'size' => 'sm', 'md', 'lg',
    'type' => 'text', 'email', 'tel', 'password', 'date', 'time', 'url', 'color', 'range',
    'placeholder' => '',
    'label' => '',
    'name' => '',
    'icon' => false,
    'disabled' => false
])

<div 
    {{-- class="flex flex-col justify-end pr-4 my-4 min-w-1/6"  --}}
    {{ $attributes->merge([ 'class' => 'my-4 pr-4 flex flex-col justify-end' ]) }}
    >
    <x-adda::label >{{ $label }}</x-adda::label>
    <input 
        class="h-14 p-2 rounded-lg adda_input adda_input-{{ $type }}"
        {{ $attributes->merge(['type' => $type]) }}
        {{-- type='' --}}
        placeholder="{{ $placeholder }} input {{ $type }}"
    >
</div>
