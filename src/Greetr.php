<?php

namespace ArgilData\ArgilBlade;

class Greetr
{
    public function greet(String $sName)
    {
        return 'Hi ' . $sName . '! How are you doing in composer my Argil Blade package?';
    }
}