<?php

namespace ArgilData\ArgilView\Facades;

use Illuminate\Support\Facades\Facade;

class Argil extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'argil';
    }
}
